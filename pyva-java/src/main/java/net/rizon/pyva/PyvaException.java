package net.rizon.pyva;

import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("serial")
public class PyvaException extends Exception
{
	public PyvaException(String message)
	{
		super(message);
	}
}

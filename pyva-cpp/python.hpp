#include <Python.h>
#include <frameobject.h>
#include "pyva.hpp"

namespace Python
{
	/* main thread state */
	extern PyThreadState *state;

	extern void Start();
	extern void Stop();

	template<typename T>
	class TypedReference
	{
		T *obj;

	 public:
		TypedReference();
		TypedReference(T *obj);
		TypedReference(const TypedReference<T> &);

		~TypedReference();

		TypedReference& operator=(const TypedReference<T> &);

		operator T*() const;
		T* operator->() const;

		T** Pointer();
	};

	using Reference = TypedReference<PyObject>;

	struct Lock
	{
		Lock();
		Lock(Pyva *);
		~Lock();
	};

	struct AllowThreads
	{
		PyThreadState *save;

		AllowThreads();
		~AllowThreads();
	};
}

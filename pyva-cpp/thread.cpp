#include "thread.hpp"

using namespace Thread;

Mutex::Mutex()
{
#ifndef WIN32
	pthread_mutex_init(&mutex, NULL);
#else
	InitializeCriticalSection(&mutex);
#endif
}

Mutex::~Mutex()
{
#ifndef WIN32
	pthread_mutex_destroy(&mutex);
#else
	DeleteCriticalSection(&mutex);
#endif
}

void Mutex::Lock()
{
#ifndef WIN32
	pthread_mutex_lock(&mutex);
#else
	EnterCriticalSection(&mutex);
#endif
}

void Mutex::Unlock()
{
#ifndef WIN32
	pthread_mutex_unlock(&mutex);
#else
	LeaveCriticalSection(&mutex);
#endif
}

Lock::Lock(Mutex &m) : mutex(m)
{
	mutex.Lock();
}

Lock::~Lock()
{
	mutex.Unlock();
}


#include <jni.h>

#include <string>
#include <vector>
#include <algorithm>

namespace JNI
{
	extern JavaVM *vm;

	extern void Start();
	extern void Stop();

	class Env
	{
		bool attached;
		JNIEnv *e;
	 public:
		Env();
		~Env();
		JNIEnv *GetEnv() const;
	};

	extern int Hash(jobject obj);

	class String
	{
		jstring jstr;
		const char *cstr;

	 public:
		String();
		String(jstring jstr);
		String(const String &);

		~String();

		String& operator=(const String &);

		operator const char *();
		operator std::string();
	};

	template<typename T>
	class LocalReference
	{
		T ref;

	 public:
		LocalReference(T ref);
		LocalReference(LocalReference &&);

		~LocalReference();

		LocalReference& operator=(LocalReference &&);

		operator T() const;
	};

	template<typename T>
	class StaticGlobalReference
	{
		T ref;

	 public:
	 	StaticGlobalReference();
		StaticGlobalReference(T ref);
		StaticGlobalReference(StaticGlobalReference &&);

		~StaticGlobalReference();

		void Release();

		StaticGlobalReference& operator=(StaticGlobalReference &&);

		operator T() const;
	};

	template<typename T>
	class GlobalReference : public StaticGlobalReference<T>
	{
	 public:
		GlobalReference(T ref);
		~GlobalReference();
	};


	extern StaticGlobalReference<jclass>
		java_lang_Object,
		java_lang_Boolean,
		java_lang_Integer,
		java_lang_Double,
		java_lang_Long,
		java_lang_String,
		java_lang_Throwable,
		net_rizon_pyva_PyvaException,
		net_rizon_pyva_Util;

	extern jmethodID
		java_lang_Object_toString,
		java_lang_Object_hashCode,
		java_lang_Boolean_init,
		java_lang_Boolean_booleanValue,
		java_lang_Integer_init,
		java_lang_Integer_intValue,
		java_lang_Double_init,
		java_lang_Double_doubleValue,
		java_lang_Long_init,
		java_lang_Long_longValue,
		net_rizon_pyva_PyvaException_init,
		net_rizon_pyva_Util_isArray,
		net_rizon_pyva_Util_getIterator,
		net_rizon_pyva_Util_hasNextIterator,
		net_rizon_pyva_Util_nextIterator,
		net_rizon_pyva_Util_getClassName,
		net_rizon_pyva_Util_getAttribute,
		net_rizon_pyva_Util_setAttribute,
		net_rizon_pyva_Util_invokeConstructor,
		net_rizon_pyva_Util_invoke,
		net_rizon_pyva_Util_getCause,
		net_rizon_pyva_Util_splitException,
		net_rizon_pyva_Util_prettyException;
}


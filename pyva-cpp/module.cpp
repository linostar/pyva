#include "python.hpp"
#include "pyva_object.hpp"

#if PY_MAJOR_VERSION >= 3
#define INIT_ERROR return nullptr

static struct PyModuleDef moduledef = {
	PyModuleDef_HEAD_INIT,
	"libpyva"
};

PyObject *
PyInit_libpva(void)
#else
#define INIT_ERROR return

PyMODINIT_FUNC
initlibpyva(void)
#endif
{
#if PY_MAJOR_VERSION >= 3
	PyObject *m = PyModule_Create(&moduledef);
#else
	PyObject *m = Py_InitModule("libpyva", nullptr);
#endif

	if (m == nullptr)
		INIT_ERROR;

	/* We can only be imported from within pyva, not directly */
	if (!(PyvaObjectType.tp_flags & Py_TPFLAGS_READY) || !(PyvaExceptionType.tp_flags & Py_TPFLAGS_READY))
	{
		PyErr_SetString(PyExc_RuntimeError, "libpyva may not be imported directly");
		INIT_ERROR;
	}

	Py_INCREF(&PyvaExceptionType);
	if (PyModule_AddObject(m, "PyvaException", reinterpret_cast<PyObject *>(&PyvaExceptionType)) < 0)
		INIT_ERROR;

#if PY_MAJOR_VERSION >= 3
	return m;
#endif
}


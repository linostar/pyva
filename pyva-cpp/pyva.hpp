#include "jni.hpp"
#include "thread.hpp"
#include <list>

class Pyva
{
 public:
	struct HashNode
	{
		int hashv;
		jobject ref;
		bool strong;
		PyObject *py;

		HashNode(int hv, jobject r, PyObject *p);
	};

 private:
#ifndef WIN32
	static constexpr size_t buckets = (1 << 16);
	using Bucket = std::list<HashNode>;
	/* Python's GIL is used to synchronize access to this */
	Bucket objects[buckets];
#else
	static const size_t buckets = (1 << 16);
	typedef std::list<HashNode> Bucket;
	Bucket objects[1 << 16];
#endif

	void SetupPyvaObject(const char *name, PyTypeObject *obj);

 public:
	/* net.rizon.pyva.Pyva object for this interpreter */
	JNI::GlobalReference<jobject> pyva;
	/* main thread state of the sub interpreter */
	PyThreadState *state;

	Pyva(jobject);
	~Pyva();

	void RegisterPyva();

	PyObject *IsCached(jobject jobj);
	bool IsStrong(jobject obj) const;
	void Cache(jobject jobj, PyObject *pyobject);
	int GC();
	void Clear();

	void AddTracebackFrame(const std::string &file, const std::string &method, int line);

	static Thread::Mutex interpreters_lock;
	static std::vector<Pyva *> interpreters;
	static Pyva *Get(jobject);
	static Pyva *Get();
};

extern jobject to_java(PyObject *obj);
extern PyObject *to_python(jobject obj);

